<?php
namespace InfoFlo;
use GFCommon;
define( 'DIR_PATH_FIELDS', plugin_dir_path( __FILE__ ) );

/**
 * Plugin Name:     InfoFloField
 * Description:     This is a test plugin
 * Author:          hmt
 * Text Domain:     infoflofields
 * Version:         0.1.0
 *
 * @package         Infoflofields
 */


// Your code starts here.
if ( ! class_exists( 'InfoFloFieldPlugin' ) ) {
    class InfoFloFieldPlugin {
        
        /**
         * Constructor
         */
        public function __construct() {
            $this->setupActions();

        }
        
        /**
         * Setting up Hooks
         */
        public function setupActions() {
            //Main plugin hooks
            register_activation_hook( DIR_PATH_FIELDS, array( 'InfoFloFieldPlugin', 'activate' ) );
            register_deactivation_hook( DIR_PATH_FIELDS, array( 'InfoFloFieldPlugin', 'deactivate' ) );
            add_action( 'gform_pre_submission', array($this, 'copyValuesByAdminLabel'), 10, 2 );
        }

        public function copyValuesByAdminLabel($form) {
            $radioFields = [];
            $textFields = [];
            foreach ($form['fields'] as $field) {
                if ($field->type == 'radio') {
                    if (strpos($field->adminLabel, 'infoflo-field-') !== false) {
                        $tmp = explode('infoflo-field-', $field->adminLabel);
                        if (!empty($tmp)) {
                            $radioFields[$tmp[1]] = $field->id;
                        }
                    }
                } elseif ($field->type == 'text') {
                    if (strpos($field->adminLabel, 'infoflo-text-field-') !== false) {
                        $tmp = explode('infoflo-text-field-', $field->adminLabel);
                        if (!empty($tmp)) {
                            $textFields[$tmp[1]] = $field->id;
                        }
                    }
                }
            }
            
            foreach ($textFields as $key => $textField) {
                if (isset($radioFields[$key])) {
                    $_POST['input_' . $textField] = $_POST['input_' . $radioFields[$key]];
                }
            }
            error_log(print_r($_POST, true));
        }


        /**
         * Activate callback
         */
        public static function activate() {
            //Activation code in here
        }
        
        /**
         * Deactivate callback
         */
        public static function deactivate() {
            //Deactivation code in here
        }
        
    }
    
    
    // instantiate the plugin class
    $wp_plugin_template = new InfoFloFieldPlugin();
}