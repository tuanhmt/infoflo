<?php

namespace InfoFlo;
use GFAPI;
define( 'DIR_PATH', plugin_dir_path( __FILE__ ) );

/**
 * Plugin Name:     InfoFlo
 * Description:     This is a test plugin
 * Author:          hmt
 * Text Domain:     infofloforms
 * Version:         0.1.0
 *
 * @package         Infofloforms
 */

// Your code starts here.
if ( ! class_exists( 'InfoFloFormPlugin' ) ) {
    class InfoFloFormPlugin {
        
        /**
         * Constructor
         */
        public function __construct() {
            $this->setupActions();

        }
        
        /**
         * Setting up Hooks
         */
        public function setupActions() {
            //Main plugin hooks
            register_activation_hook( DIR_PATH, array( 'InfoFloFormPlugin', 'activate' ) );
            register_deactivation_hook( DIR_PATH, array( 'InfoFloFormPlugin', 'deactivate' ) );
            add_shortcode('infofloforms', array($this, 'infofloFormsShortCode'));
            add_action('wp_ajax_filter_infofloform', array($this, 'getFormWithFitler')); // wp_ajax_{ACTION HERE} 
            add_action('wp_ajax_nopriv_filter_infofloform', array($this, 'getFormWithFitler'));
            add_action('wp_enqueue_scripts', array($this, 'infofloformsScripts'));
            
        }

        public function infofloformsScripts() {
            wp_enqueue_script('custom', plugins_url() . '/infofloforms/js/custom.js', array( 'jquery' ));
            // The second parameter ('aj_ajax_url') will be used in the javascript code.
            wp_localize_script( 'custom', 'aj_ajax_demo', array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'aj_demo_nonce' => wp_create_nonce('aj-demo-nonce')
            ));
        }

        public function getInfofloFormsFilter() {
            return '<form action=' . site_url() . '/wp-admin/admin-ajax.php" method="POST" id="filter">
                        <div class="form-check filter-active form-group">
                            <input class="form-check-input" type="checkbox" name="activeCheck" id="activeCheck">
                            <label class="form-check-label" for="activeCheck">
                            Show inactive ?
                            </label>
                        </div>
                    </form>';
        }

        /**
         * Undocumented function
         *
         * @param array $param
         * @return void
         */
        public function getFormWithFitler() {
            check_ajax_referer( 'aj-demo-nonce', 'nonce' );
            $in_active = isset($_POST['in_active']) ? filter_var($_POST['in_active'], FILTER_VALIDATE_BOOLEAN) : false;
            $in_active = !$in_active;
            $results = GFAPI::get_forms($in_active);

            // Return json result for jquery ajax.
            if (!empty($results)) {
                wp_send_json($results);
            }

            wp_send_json_error();
            wp_die();
        }

        /**
         * Infofloforms shortcode implementation.
         */
        public function infofloFormsShortCode($attrs = []) {
            // normalize attribute keys, lowercase
            $attrs = array_change_key_case( (array) $attrs, CASE_LOWER );
            $result = '';
            $forms = [];
            if (isset($attrs['form_id']) && is_numeric($attrs['form_id'])) {
                $forms[] = GFAPI::get_form($attrs['form_id']);
            } else {
                $forms = GFAPI::get_forms();
                $result = $this->getInfofloFormsFilter();
            }


            $result .= '<div id="' . $attrs['id'] . '" class="infolo-wrapper">';
            $result .= '<table class="table table-bordered">';
            $result .= '<thead class="thead-light">';
            $result .= '<tr>';
            $result .= '<th>ID</th>';
            $result .= '<th>Title</th>';
            $result .= '<th>Created</th>';
            $result .= '<th>Active</th>';
            $result .= '</tr>';
            $result .= '</thead>';
            $result .= '<tbody id="' . $attrs['id'] . '-body">';
            foreach ($forms as $form) {
                $result .= '<tr>';
                $result .= '<td>' . $form['id'] . '</td>';
                $result .= '<td>' . $form['title'] . '</td>';
                $result .= '<td>' . $form['date_created'] . '</td>';
                $result .= '<td>' . $form['is_active'] . '</td>';
                $result .= '</tr>';
            }
            $result .= '</tbody>';
            $result .= '</table>';
            $result .= '</div>';
            return $result;
        }
        
        /**
         * Activate callback
         */
        public static function activate() {
            //Activation code in here
        }
        
        /**
         * Deactivate callback
         */
        public static function deactivate() {
            //Deactivation code in here
        }
        
    }
    
    
    // instantiate the plugin class
    $wp_plugin_template = new InfoFloFormPlugin();
}
