(function($) {
    $(document).ready(function(){
        $('input[name=activeCheck]').on('click', function(e) {
            var in_active = $("input[name=activeCheck]:checked").val();
            if (in_active === undefined) {
                in_active = false;
            } else {
                in_active = true;
            }
            
            $.ajax({
                url : aj_ajax_demo.ajax_url, // Note that 'aj_ajax_demo' is from the wp_localize_script() call.
                type : 'post',
                data : {
                    action : 'filter_infofloform',  // Note that this is part of the add_action() call.
                    nonce : aj_ajax_demo.aj_demo_nonce,  // Note that 'aj_demo_nonce' is from the wp_localize_script() call.
                    in_active : in_active
                },
                success : function( response ) {
                    $('#infofloforms-body').html('');
                    if (response.success !== false) { 
                        $.each(response, function (key, value) {
                            $('#infofloforms-body').append("<tr>\
                                <td>"+value.id+"</td>\
                                <td>"+value.title+"</td>\
                                <td>"+value.date_created+"</td>\
                                <td>"+value.is_active+"</td>\
                            </tr>");
                        })
                    }
                },
                error : function( response ) {
                    alert('Error retrieving the information: ' + response.status + ' ' + response.statusText);
                }
            });
        });
    });
}(jQuery));